package oculus.tablist.bukkit;

import com.earth2me.essentials.Essentials;
import me.clip.placeholderapi.PlaceholderAPI;
import oculus.tablist.utils.NumberUtil;
import oculus.tablist.utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BukkitMain extends JavaPlugin implements Listener {
    private static Timer timer;
    private static Essentials Essentials;
    private static boolean hasPlaceholderAPI;
    private static BukkitMain intance = null;

    public static int getPlayerCount() {
        int count = 0;
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!isVanished(p)) {
                count++;
            }
        }
        return count;
    }

    public static boolean isVanished(Player p) {
        for (MetadataValue meta : p.getMetadata("vanished")) {
            if (meta.asBoolean()) return true;
        }
        if (Essentials != null) {
            return Essentials.getUser(p).isVanished();
        }
        return false;
    }

    public static String format(Player p, String str) {
        Date date = new Date();
        final ChatColor color;
        final double tps = timer.getAverageTPS();
        if (tps >= 18.0) {
            color = ChatColor.GREEN;
        } else if (tps >= 15.0) {
            color = ChatColor.YELLOW;
        } else {
            color = ChatColor.RED;
        }
        str = ChatColor.translateAlternateColorCodes('&',str)
                .replace("{TIME}", new SimpleDateFormat("HH:mm:ss").format(date))
                .replace("{DATE}", new SimpleDateFormat("dd.MM.yyyy").format(date))
                .replace("{TPS_COLOR}", color.toString())
                .replace("{TPS}", String.valueOf(NumberUtil.formatDouble(tps)))
                .replace("{PLAYERCOUNT}", String.valueOf(getPlayerCount()))
                .replace("{MAXPLAYERS}", String.valueOf(Bukkit.getMaxPlayers()));

        if (hasPlaceholderAPI) {
            str = PlaceholderAPI.setPlaceholders(p, str);
        }

        return str;
    }

    public static void updatePlayerListHeaderFooter() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.setPlayerListHeaderFooter(
                    format(p, StringUtils.join("§r\n", getInstance().getConfig().getStringList("Tablist.Header"))),
                    format(p, StringUtils.join("§r\n", getInstance().getConfig().getStringList("Tablist.Footer")))
            );
        }
    }

    public static Timer getTimer() {
        return timer;
    }

    public static BukkitMain getInstance() {
        return BukkitMain.intance;
    }

    public void onEnable() {
        new Metrics(this, 9987);

        BukkitMain.intance = this;

        saveDefaultConfig();
        reloadConfig();

        PluginManager pluginManager = Bukkit.getServer().getPluginManager();
        Essentials = (Essentials) pluginManager.getPlugin("Essentials");
        hasPlaceholderAPI = pluginManager.getPlugin("PlaceholderAPI") != null;

        pluginManager.registerEvents(this, this);

        timer = new Timer();
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, timer, 1000, 50);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, BukkitMain::updatePlayerListHeaderFooter, 0, 20);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        updatePlayerListHeaderFooter();
    }

}
